import requests
import json

post_code = input('Please supply a post code \n > ').strip()

base_url = "http://api.postcodes.io/postcodes/"

request_response = requests.get(base_url + post_code)

# print(request_response)

# print(request_response.content)

# print(request_response.json())

print(request_response.json().keys())

json_obj_response = request_response.json()

print('longitude:', json_obj_response['result']['longitude'])
print('latitude:', json_obj_response['result']['latitude'])
print('nuts code:', json_obj_response['result']['codes']['nuts'])


# what if we want 3 post codes? 
## You can use a POST request
### Post request have url + path + argument  AND can take a JSON object
### (ALL request also have headers AKA metadata)

