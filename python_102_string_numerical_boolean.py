# Strings
# A list of unorganised characters that can have spaces, numbers and any characters.
# syntax "" or ''
print('this is a string')
print(type("this too is a string"))

my_string = "thIS is A nICe simple String"

# len() -> legenth of string
print(len(my_string))
print(my_string)

# Useful string methods
# objct.capitalize() -> make the first character upper case
print(my_string.capitalize())

# .lower() --> all charaters lower
print(my_string.lower())

# .upper() --> Makes it upper case
print(my_string.upper())

# .title()
print(my_string.title())


# Concactenation of strings
## the joining of two strings

name = "Filipe"
greeting = 'Howdyieh'

print(greeting + name)
print(greeting + ' ' + name)
print(greeting, name)


# Interpolation with f strings
# you can interpolate into a string a value.
# syntax print(f"{}")

print("Welcome and howdy <name>!")
print(f"Welcome and howdy {name}!")
print(f"Welcome and howdy {name.upper()}!")

# String.. as said in the start. Are lists. of Characters. But they are list.
print(name[1]) # don't worry too much about this 

# Numerical types
# These are numbers. We have integers, floats, and a few others that are less used.
# you can perform mathemathical arithatic with them. 

my_number = 16
print(my_number)
print(type(my_number))

num_a = 8
num_b = 24

# Add, subtract, divide, multiply and others 

print(num_a + num_b)
print(num_a - num_b)
print(num_b * 1000020202)
# All of the above are intergers.
## just a whole number with out .0202
print(num_b/2)
print(type(num_b/2))

# Floats are composite numbers, everythime you devide you also get a float
print(type(3.14))

## There are comparison operators that you can use logicially with maths.
## such as greater than, smaller than, and so on. 
# the result is what is called a boolean 

# A bollean is a data type is either True or False. 
# Booleans - True or False

print(type(True)) # <class 'bool'>
print(type(False)) # <class 'bool'>

## Comparison Operators 
num_a = 8
num_b = 24

# greater than
print(num_a > num_b) # False
print(type(num_a > num_b))

# Smaller than
print(num_a < num_b)

# greater than or equal
print(num_a >= 8)
print(num_a >= num_b)

# Smaller than or equal 
print(num_a <= 8)
print(num_a <= num_b)

# Equates to sides
print(num_a == num_b)
# one = is assignment, hence most languages use == or === (if your js and your == is broken)

# Not equal
print(num_a != num_b)
# Not equal


## Comparison operator also work for strings
print('is the string 10 equal to integer 10 in py?', '10' == 10)

example_input = 10 
user_input=input("> provide input:  ")
print(example_input == user_input)
print(type(example_input))
print(type(user_input))

# Casting
## User input is captured as a string
## helps us change data type where possible
## very useful when taking in user input.

## Change a integer to string 
# str() --> str 

example_int = 101
example_srt = str(example_int)
print(type(example_srt))

# ## change str into an integer 
# # Will only work with clean strings using numerical types only 
print(type(int('10')))




