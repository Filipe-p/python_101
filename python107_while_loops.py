# While loops 
# A loop with a condition 
# Don't make endless loops! Like deviding by zero

# Syntax - break condition at top
# while <condtiond>:
    # block of code
    # block of code


# Syntax - break condition in body with break.
# while True:
    # block of code
    # block of code
    # <conditon>:
        # break

# counter = 0
# while counter < 10:
#     print("I'm in the while loooppp")
#     print(counter)
#     counter += 1

# counter = 0
# while True:
#     print("I'm in the while loooppp")
#     if counter > 10:
#         break
#     counter += 1

## Create small whitch boards - good for small games/apps 

while True:
    print("Welcome to the matrix")
    print("Press 1 for hello")
    print("Press 2 for a surprise")
    print("Press 3 for goodbey")
    user_input = int(input("Please choose an option").strip())

    # use user input to control flow what actions 
    if user_input == 1:
        print("\n \n")
        print("howdy partner")
        print("\n \n")
    elif user_input == 2:
        print("\n \n")
        print("💩")
        print("\n \n")
    elif user_input == 3:
        print("\n \n")
        print("goodbey")
        print("\n \n")
        break
    else:
        print("you must choose between 1-3")

    