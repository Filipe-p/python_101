# Functions
# Functions are like machine, they can take in arguments, do some work and output a value.
# To work, functions must be called. 

# Concepts: 
# DRY - Don't Repeat Yourself 
    # Avoid reptetion in your code - functions can help
    # Maintainable

# Good functions: 
    # Are like people - They should have 1 job!
    # makes it easier to manage
    # Makes it esier to measure
    # And test
    # IMPORTANT - Don't print inside a function. you return 

#syntac
# def <function_name>(arg1, arg2, arg*):
    # Block_code
    # Block_code
    # return <value>

#defining function 
def say_hello():
    return "helloooooo"

#calling functions
# say_hello()
# print(say_hello())

# Function with arf
def say_hello_human(human):
    return "hellooooooooo " + human

# print(say_hello_human('Koffiiii'))


def full_name_user_ask():
    f_name = input('> provide a first name: \n >').strip().capitalize()
    l_name = input('> provide a last name: \n >').strip().capitalize()

    return f_name + ' ' +l_name

# print(full_name_user_ask())


# You can have defaults

def full_name(f_name="Alan", l_name="Turin"):
    return f_name + ' ' + l_name

print(full_name())
print(full_name('Filipe'))
print(full_name(l_name='Paiva'))