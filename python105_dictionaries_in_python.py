## Dictionaries in Python
# Allow us to keep complex data. 
    # mike + his number and address
    # anna and her company 
    # Other people 
# It's very simple, It works like a real dictionary. 
# it jumpts to the words you are looking for and has a value for it.

# Syntax
# {}
# {"key" : value}
print(type({}))

example_dict = {
    "human1" : "Aike",
    "human2" : "Anna" 
}

# print the dictionary 
print(example_dict)

# use the keys to get values - Like a list, but with keys
print(example_dict["human2"])

# Reassing values 
example_dict["human2"] = "Arnold"
print(example_dict)

# Make key value pairs on the fly. 
example_dict["human47"] = "Secrete Agente 047"
print(example_dict)

## If you just want .keys() or the .values()

print(example_dict.keys())
print(example_dict.values())


### 
person_1 = {
    "name" : "Britney",
    "Age" : 30,
    "Skills": "Media"
}

person_2 = {
    "name" : "Justin Timberlake",
    "Age" : 34,
    "Skills": "Other"
}

list_of_dictionaries = [person_1, person_2]

print(type(list_of_dictionaries))
print(list_of_dictionaries)

