## print() command 
## print command makes code output to the terminal. Super useful for debugging :D

print('hello world!')

## ^^this does not mean that code cannot run without outputting to the terminal 
10*100
20*33
#print(20*2) # the only one that get printed

## type()
# Help you identify the type of data, What class?. For example:
print(10)
print(type(10))

print('10')
print(type('10'))

## Variables
# A variable is like a box. It has a name. We can put stuff inside, and then get stuff that is inside.
# We can also at any point put other stuff inside the same box. 
# we could have a box called books, and put "Rich Dad, Poor Dad" inside

# Assignment of variable books to a string
books = "Rich dad, Poor Dad"

# calling of the variable
print(books)
print(type(books))


## prompting user for input
# syntax input('message')

input('Tell me a numbaaahh ')

# you need to capture the input into a variable to use later

user_var = input('Tell me a numbaaahh again :D ')
print(user_var)


## Python Erros 
### they Always point you to the line that is broken + give you the type of error.
# Syntaxerro
# no name error
# Wrong data type 
