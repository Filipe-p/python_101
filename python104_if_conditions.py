# if Condtions 
# if conditions are part of control flow.
## When a condition becomes True it runs a block of code.
## There is also a option to program for an else situation when no codition has become true.

# The syntax 
# Example

if True:
    print('it is true')
elif True:
    print("please print me")
else:
    print('I will not be printed')

## New example for the else 
if False:
    print('I will not be printed')
else: 
    print('printing from the else')

weather = "Windy"
# weather = input('what is the weather  ').strip().lower()

print(f"{weather}")
print(weather == "windy")
# Usually you want to treat user input and format for matching 

if weather == "sunny":
    print(f'the weather is {weather}')
    print("take shades")
elif weather == "windy":
    print('Weather is windy')
    print('Fly a kite')
else:
    print('Weather is not nice')
    print('take a coat')


## Matching with 'in'
# Python has a "in" matcher that allows you to match with less restriction
# Works in list, numbers and strings 

loterry_number = 128
your_numbers = [10, 120, 30, 50, 140, 128]

print(128 in your_numbers)
print(loterry_number in your_numbers)

magic_word = "please"

print(magic_word in "Where are the Cookies?") 

user_question = input("> what would you like?")

if magic_word in user_question:
    print("you shall have what you whish")
else:
    print("you did not use the magic word...")