# Python 101 

Python is a programing language that is syntax light and easy to pick up. It is very versatile and well paid. Used across the industry for:

- simple scripts
- Automation (ansibles)
- Web Development ()
- Data and ML
- Others

List of things to cover:

- Data types
- Strings
- Numericals
- Booleans
- List
- If conditions
- Logical Or and logical and
- Dictionaties
- Loops
- While loops
- Embed lists and dictionaries
- Debugers
- Functions
- TDD & Unitesting
- Error handling
- External packages
- API's


Other topics to talk:

- Polymorphsim
- TDD
- DRY code
- Seperation of concerns
- ETL - Extract transform and load
- Frameworks (difference to actual languages)


## Python Intro a basics

Python you can make files and run them, you can run on the command line, you can have a Framework running python or use an IDE.

To try stuff out super quick, use the comand line.

```bash 
python3
>>>>
```

Then you can write python.

```python
>>> human = "Mike Page"
>>> print(human)
Mike Page

```


## Virtual Environment in python venv

.venv folder is a virtual environment for python, where you can install packages with pip locally per project, rather than globally in your machine. 

You need to start a venv environment and activate it, using ´source´.

code

```bash

python3 -m venv .venv
source .venv/bin/activate

```

This  is to avoid package colision on your machine and contains the python packages installed to this folder. 

**requierments.txt** Is a text file where you can specify a list of packages your code will need. You can then load it / read it using pip. It also lock in Versions. 

read it with pip or pip3 using this code:

```bash 
pip install -r requirements.txt
```

This will install all the packages in requirements.txt

