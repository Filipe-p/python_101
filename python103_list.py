## List
# https://docs.python.org/3/library/stdtypes.html#sequence-types-list-tuple-range
# A list a numerically organised list of objects. Lists are organised using indexes, that start at 0.
# A list can be anything, like a shopping list or list of crazy x partners/employers/landlords

# syntax []
print(type([]))

# You can define a list with seveal items
my_crazy_employers = ['Walt Disney', "Loony toons", "CartonNetwork", "Panda Channel"]

# Call the list / print everything
print(my_crazy_employers)

# List are organised with index - starting at 0
my_crazy_employers = ['Walt Disney', "Loony toons", "CartonNetwork", "Panda Channel"]
# list_name        = [      0      ,      1       ,        2       ,         3   ]

# use index to get individual items of a list 
print(my_crazy_employers[0])
print(my_crazy_employers[1])
print(my_crazy_employers[-1])

## Add something to the list
# .append()
my_crazy_employers.append('Channel 4')
print(len(my_crazy_employers))

## Remove the last from list
# .pop()
my_crazy_employers.pop()
print(len(my_crazy_employers))

## Remove using index and delete 
my_crazy_employers.pop(1)
print(my_crazy_employers)

## List are Mutable and can take multiple data types

new_list = [10, 11, 20, "hello", "multiple data", [10, 20, "yoh"]]

print(new_list)
print(type(new_list[0]))
print(type(new_list[3]))
print(type(new_list[5]))

# List are mutable and you can re-assign a index position 
new_list[1] = 2000000000
print(new_list)