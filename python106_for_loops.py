# For loops 

# Syntax 
# for place_holder in <iterable>:
    # Block_of_code

farhiaya_gifts = ['Roller Blades', 'Roller Skates', 'Vacay :D']

for item_placeholder in farhiaya_gifts:
    print(item_placeholder)


## Embeded lists

farhiaya_gifts_2 = [['Roller Blades', 'Roller Skates', 'Vacay :D'], ['Stuffed toys', 'gameboy', 'cool bike']]

for list_item in farhiaya_gifts_2:
    print(list_item)
    for item in list_item:
        print(item)


## Lest say we want to create a string with all the items
farhiaya_gifts_2 = [['Roller Blades', 'Roller Skates', 'Vacay :D'], ['Stuffed toys', 'gameboy', 'cool bike']]

# Let's first iterate over the list
# Then get each individual item and add it to a string 
# Let's start a new variable to hold said string

result_s = ""

for list_list in farhiaya_gifts_2:
    print(list_list)
    for item in list_list:
        result_s = result_s + " " + item

print(result_s.strip())

print("^^^^^^^^^^^^^^^^^^^ ")
## Iterating over a Dictionary
person_2 = {
    "name" : "Justin Timberlake",
    "Age" : 34,
    "Skills": "Other"
}
print(person_2)
for key_anything in person_2:
    print(key_anything)
    # how do I get the values? :) dictionary + key
    print(person_2[key_anything])

# print("^^^^^^^^^^^^^^^^^^^ ")

for filipe in person_2.keys():
     print(person_2[filipe])

